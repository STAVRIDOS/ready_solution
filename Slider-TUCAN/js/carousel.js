var row = document.querySelector(".row-all"),
sl = row.querySelectorAll(".sl"),
count,
step,
steps = [],
sliders,
slider,
blocksAll,
blockWidth,
colBlock,
active_link,
circle,
num = 0,
nums = [],
indexBlock = 0,
indexArray;

row.addEventListener("click",click);



(function(){	
	for(var b=0;b < sl.length;b+=1){
		var rs = sl[b].querySelector('.row-slider');
		var num = rs.querySelectorAll('.myslide');
		colBlock = num.length;
		var count = +rs.dataset.col;
		navigations(sl[b],num);
		createCircle(sl[b],colBlock,count);
		sl[b].setAttribute("data-number", b)
		steps.push(0)
		nums.push(0)
	}
}())

function click(e){
		row_block = e.target.closest(".sl");
		indexArray = row_block.dataset.number;
		step = steps[indexArray];
		cirCol = row_block.querySelectorAll(".circle");
		if(row_block.querySelector('.active-link')){
			block = row_block.querySelector(row_block.querySelector('.active-link').getAttribute('href'));
		}else{
			block = row_block.querySelector('.slider');
		}
		slider = block.querySelector(".row-slider");
		count = colSlider();
		blockWidth = widthOneBlock(block);
		colBlock = colBlocks(block);
		num = nums[indexArray];
if(e.target.getAttribute("class") === "rightButton"){
		slideRight(slider,blockWidth,colBlock,indexArray);
	}else if(e.target.getAttribute("class") === "leftButton"){
		slideLeft(slider,blockWidth,colBlock,indexArray);
	}else if(e.target.parentNode.getAttribute("class") === "buttons-navigations"){
		circle = row_block.querySelectorAll(".block-circle");
		tab(e);
		(circle.length > 0) ? circle[0].remove() : "";
		createCircle(row_block,colBlock,count);
		nums[indexArray] = 0;
	}else if(e.target.parentNode.getAttribute("class") === "block-circle"){
		var round = +e.target.dataset.index;
		if(num <= round){
			var onr = round-num;
			for(var i = 0; i < onr; i++){
				slideRight()
			}
		}else{
			var onl = num-round;
			for(var i = 0; i < onl; i++){
				slideLeft()
			}
		}
		num = round;
		activeCircle()
	}
	steps[indexArray] = step;
}

function tab(e){
	step = 0;
	slider.style= "margin-left: 0px";
}

function slideLeft(){
	num = num-1;
	if(step <= 0){
		step = colBlock*blockWidth-(blockWidth*count);
		slider.style= "margin-left:-"+ step +"px";
	}else{
		step -= blockWidth;	
		slider.style= "margin-left:-"+ step +"px";
	}
	activeCircle(indexArray)
}

function slideRight(){
	num = num+1;
	if(step < colBlock*blockWidth-(blockWidth*count)){
			step += blockWidth;
			slider.style = "margin-left:-"+ step +"px";
		}else{
			step = 0;
			slider.style = "margin-left:-"+ step +"px";
		}
		activeCircle(indexArray)
}

function navigations(appendElement,blocksAll){
	var blockButton = document.createElement("div");
	var leftButton = document.createElement("span");
	var rightButton = document.createElement("span");
	blockButton.classList.add('block-button');
	leftButton.classList.add('leftButton');
	rightButton.classList.add('rightButton');
	blockButton.appendChild(leftButton);
	blockButton.appendChild(rightButton);
	appendElement.appendChild(blockButton);
};


function colBlocks(block){
	var col = block.querySelectorAll(".myslide").length;
	return col;
}
function colSlider(){
	if(slider.dataset.col != count){
		step = 0;
		count = slider.dataset.col;
	}else{
		count = slider.dataset.col;
	}
	return count;
}

function widthOneBlock(block){
	var col = block.querySelectorAll(".myslide");
	return col[0].offsetWidth;
}

function createCircle(appendElement,colBlock,count){
	var blockCircle = document.createElement("div");
			blockCircle.classList.add('block-circle');
		for(var i=0;i< colBlock-count+1;i++){
			var circleOne = document.createElement("span");
				(i == 0) ? circleOne.classList.add('active-circle') : "";
				circleOne.classList.add('circle')
				circleOne.setAttribute("data-index",i)
				blockCircle.appendChild(circleOne);
		}
		appendElement.appendChild(blockCircle);
}

function activeCircle(){
	if(num >= cirCol.length){
		num = 0;
	}else if(num < 0){
		num = cirCol.length-1;
	}
	for(var i=0; i < cirCol.length; i+=1){
		if(i == num){
			cirCol[i].classList.add("active-circle")
		}else{
			cirCol[i].classList.remove("active-circle")
		}
	}
	nums[indexArray] = num;
}